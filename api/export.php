<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
set_time_limit(0);
require "../vendor/autoload.php";

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;
$capsule->addConnection([
    "driver" => "mysql",
    "host" => "158.85.72.153",
    "database" => "semrush",
    "username" => "semrush",
    "password" => "semRush_2018_",
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
]);
//Make this Capsule instance available globally.
$capsule->setAsGlobal();

// Setup the Eloquent ORM.
$capsule->bootEloquent();
///////////////////////////////////

$models = [
    'Download',
   // 'Email',
   /* 'Form',
    'Map',
    'Manual',
    //'Negative',
    'Pdf',
    'Recipe',*/
];


$str = 'ăѣ𝔠ծềſģȟᎥ𝒋ǩľḿꞑȯ𝘱𝑞𝗋𝘴ȶ𝞄𝜈ψ𝒙𝘆𝚣1234567890!@#$%^&*()-_=+[{]};:\'",<.>/?~𝘈Ḇ𝖢𝕯٤ḞԍНǏ𝙅ƘԸⲘ𝙉০Ρ𝗤Ɍ𝓢ȚЦ𝒱Ѡ𝓧ƳȤѧᖯć𝗱ễ𝑓𝙜Ⴙ𝞲𝑗𝒌ļṃŉо𝞎𝒒ᵲꜱ𝙩ừ𝗏ŵ𝒙𝒚ź1234567890!@#$%^&*()-_=+[{]};:\'",<.>/?~АḂⲤ𝗗𝖤𝗙ꞠꓧȊ𝐉𝜥ꓡ𝑀𝑵Ǭ𝙿𝑄Ŗ𝑆𝒯𝖴𝘝𝘞ꓫŸ𝜡ả𝘢ƀ𝖼ḋếᵮℊ𝙝Ꭵ𝕛кιṃդⱺ𝓅𝘲𝕣𝖘ŧ𝑢ṽẉ𝘅ყž1234567890!@#$%^&*()-_=+[{]};:\'",<.>/?~Ѧ𝙱ƇᗞΣℱԍҤ١𝔍К𝓛𝓜ƝȎ𝚸𝑄Ṛ𝓢ṮṺƲᏔꓫ𝚈𝚭𝜶Ꮟçძ𝑒𝖿𝗀ḧ𝗂𝐣ҝɭḿ𝕟𝐨𝝔𝕢ṛ𝓼тú𝔳ẃ⤬𝝲𝗓1234567890!@#$%^&*()-_=+[{]};:\'",<.>/?~𝖠Β𝒞𝘋𝙴𝓕ĢȞỈ𝕵ꓗʟ𝙼ℕ০𝚸𝗤ՀꓢṰǓⅤ𝔚Ⲭ𝑌𝙕𝘢𝕤';

$regex = '/(
    [\xC0-\xC1]
    | [\xF5-\xFF]
    | \xE0[\x80-\x9F]
    | \xF0[\x80-\x8F]
    | [\xC2-\xDF](?![\x80-\xBF])
    | [\xE0-\xEF](?![\x80-\xBF]{2})
    | [\xF0-\xF4](?![\x80-\xBF]{3})
    | (?<=[\x0-\x7F\xF5-\xFF])[\x80-\xBF]
    | (?<![\xC2-\xDF]|[\xE0-\xEF]|[\xE0-\xEF][\x80-\xBF]|[\xF0-\xF4]|[\xF0-\xF4][\x80-\xBF]|[\xF0-\xF4][\x80-\xBF]{2})[\x80-\xBF]
    | (?<=[\xE0-\xEF])[\x80-\xBF](?![\x80-\xBF])
    | (?<=[\xF0-\xF4])[\x80-\xBF](?![\x80-\xBF]{2})
    | (?<=[\xF0-\xF4][\x80-\xBF])[\x80-\xBF](?![\x80-\xBF])
)/x';

//$regex = '#((?![а-ЯЁёАa-z 0-9"\'`~@\#!$%^\|\\\/\;:.&*,=<>_:\(\)\[\]\-+\n\t\rÀ-ž\xEF]).)*#xi';
$regex = '#((?![Аa-z 0-9:\(\)\[\]\-+\n\t\r\xEF]).)*#xi';


$chunk_size = 250000;
//$chunk_size = 25000;
foreach ($models as $model) {
    //$collected = [];
    $fp = fopen(__DIR__ . '/export/' . $model . '.csv', 'w');
    $model_name = 'App\\' . $model;
    $base_obj = new $model_name();
    $chunk_count = ceil($base_obj::where('country', '=', 'France')->orWhere('country', '=', 'Germany')->count() / $chunk_size);
    $start_id = 0;
    $to_del = [];
    for ($i = 1; $i <= $chunk_count; $i++) {
        echo "chunk $i / $chunk_count\n";
        dump($start_id);
        $data = $base_obj::where('id', '>', $start_id)->where('country', '=', 'France')->orWhere('country', '=', 'Germany')->limit($chunk_size)->get()->toArray();
        $start_id = last($data)['id'];
        foreach ($data as $datum) {
            if (preg_match($regex, trim($datum['keyword']), $match)) {
                if (count($match) == 1 && strlen($match[0]) == 0) continue;
                for ($j = 0; $j < strlen($match[0]); $j++) {
                    dump(ord($match[0][$j]));
                }
                //$collected[] = $datum;
                fputcsv($fp, $datum);
            }
        }
    }
    echo "writen $model\n";
    //$fp = fopen(__DIR__ . '/export/' . $model . '.csv', 'w');
    /*foreach ($collected as $fields) {
        fputcsv($fp, $fields);
    }*/
    fclose($fp);
}
