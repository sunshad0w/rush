<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
set_time_limit(0);
require "../vendor/autoload.php";

use App\Negative;
use Hhxsv5\PhpMultiCurl\Curl;
use Hhxsv5\PhpMultiCurl\MultiCurl;
use Illuminate\Database\Capsule\Manager as Capsule;

//$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
//socket_bind($socket, '127.0.0.1', 99999);
$sock = socket_create_listen(9999);

$capsule = new Capsule;
$capsule->addConnection([
    "driver" => "mysql",
    "host" => "158.85.72.153",
    "database" => "semrush",
    "username" => "semrush",
    "password" => "semRush_2018_",
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
]);
//Make this Capsule instance available globally.
$capsule->setAsGlobal();

// Setup the Eloquent ORM.
$capsule->bootEloquent();

//$request_body = file_get_contents('php://input');
//$data = json_decode($request_body, true);

function compareWithTable($rush_data, $negatives, $database, $country_name, $prefix)
{
  $skips = [];
  $chunk_size = 250000;
  $modelname = 'App\\' . $database;
  $base_obj = new $modelname();

  $skip_count = 0;

  $tmp = [];
  foreach ($rush_data as $key => $rush_datum) {
    if ($key == 0) {
      $skip_count++;
      continue;
    };
    $rush_row_data = array_map('trim', explode(';', $rush_datum));
    if (count($rush_row_data) !== 2) {
      //var_dump('columns skip',$rush_row_data);
      //$skips[] = ['reason'=>'column count','data'=>$rush_row_data];
      $skip_count++;
      continue;
    };
    $tmp[] = ['keyword' => $rush_row_data[0], 'country' => $country_name, 'volume' => $rush_row_data[1]];
  }
  $rush_data = $tmp;
  unset($tmp);

  $updates = [];

  $chunk_count = ceil($base_obj::where('country', '=', $country_name)->count() / $chunk_size);
  $start_id = 0;
  for ($i = 1; $i <= $chunk_count; $i++) {
    $cache_file_name = __DIR__ . '/tmp/' . $prefix . '_' . $i;
    if (file_exists($cache_file_name)) {
      $data = include($cache_file_name);
    } else {
      $data = $base_obj::where('country', '=', $country_name)->where('id', '>', $start_id)->limit($chunk_size)->get()->toArray();
      file_put_contents($cache_file_name, "<?php return " . var_export($data, true) . ";");
    }
    $start_id = last($data)['id'];
    $ex = [];
    foreach ((object)$data as $item) {
      $ex[mb_strtolower(trim($item['keyword']))] = [
        'id' => $item['id'],
        'volume' => $item['volume'],
        'created_at' => $item['created_at'],
        'keyword' => $item['keyword'],
        'country' => $item['country']
      ];
    }
    unset($data);

    $news = [];

    foreach ($rush_data as $key => $rush_row_data) {
      $hash = mb_strtolower($rush_row_data['keyword']);
      $skip = false;
      $negatived = '';
      foreach ($negatives as $negative) {
        if (!$skip && preg_match('#(^|\n|\r|\s)' . $negative . '(\:|\?|\;|\.|\s|\,|\n|\r|$)#i', $hash)) {
          $negatived = $negative;
          $skip = true;
          //var_dump($negative, $hash);
          //exit();
        }
      }
      if ($skip) {
        $skip_count++;
        if (!isset($skips[$negatived])) {
          $skips[$negatived] = 1;
        } else {
          $skips[$negatived]++;
        }
        $skips[] = ['reason'=>'negative skip','keyword'=>$rush_row_data['keyword'], 'negative'=>$negatived];

        continue;
      };
      if (isset($ex[$hash])) {
        if ($ex[$hash]['volume'] != $rush_row_data['volume']) {
          $updates[] = [
            'id' => $ex[$hash]['id'],
            'volume' => $rush_row_data['volume'],
            'created_at' => $ex[$hash]['created_at'],
            'keyword' => $ex[$hash]['keyword'],
            'country' => $ex[$hash]['country']
          ];
        } else {
          //$skips[] = ['reason'=>'volume skip','data'=>$rush_row_data];
          //var_dump('volume skip',$rush_row_data);
          $skip_count++;
          continue;
        }
      } else {
        $news[] = $rush_row_data;
      }
    }
    $rush_data = $news;
  }

  for ($i = 1; $i <= $chunk_count; $i++) {
    $cache_file_name = __DIR__ . '/tmp/' . $prefix . '_' . $i;
    if (file_exists($cache_file_name)) {
      unlink($cache_file_name);
    }
  }

  return ['new' => $rush_data, 'updates' => $updates, 'skipped' => $skip_count, 'skip'=>$skips];
}

$negs = Negative::all()->pluck('keyword')->toArray();

$list = array_diff(scandir(__DIR__ . '/list'), ['.', '..']);
sort($list);
foreach ($list as $file) {
  var_dump($file, filesize(__DIR__ . '/list/' . $file) / 1024 / 1024);
  preg_match('#(.*)_\(.*#', $file, $match);
  $exp = explode('_',end($match));
  $database = $exp[0];
  $domain = $exp[2];
  switch ($exp[2]){
    case 'fr':
      $country_name = 'France';
      break;
    case 'de':
      $country_name = 'Germany';
      break;
    default:
      $country_name = 'United States';
      break;
  }


  $to_log = [];
  $to_file = [];
  $prefix = $database . '_' . (round(time() / 60) * 60);

  $filename_relative = '/csvs/' . $file.'.csv';
  $filename = dirname(__DIR__) . $filename_relative;

  $updates = [];
  $news = [];
  $skipped = 0;

  $only_part = '';
  $body = trim(file_get_contents(__DIR__.'/list/'.$file));
  if (preg_match('/ERROR.*/', $body)) {
    preg_match('/ERROR (\d+)/', $body, $match);
    if (count($match) > 1) $only_part = $match[1];
    var_dump('error PART COUNT');
    //continue;
    //file_put_contents($error_filename, $body . "\n", FILE_APPEND);
  } else if (preg_match('/\<\!DOCTYPE html\>/', $body)) {
    preg_match('#\<title\>(.*)<\/title\>#', $body, $match);
    if (count($match)>1) $only_part = $match[1];
    //file_put_contents($error_filename, $body . "\n", FILE_APPEND);
    var_dump('error DOM');
    //continue;
  } else {
    $result = array_unique(array_map('mb_strtolower', array_map('trim', explode("\n", $body))));
    unset($body);

    if (count($result) > 1) {
      $negatives = array_map('mb_strtolower', array_map('trim', $negs));

      $compare_results = compareWithTable($result, $negatives, $database, $country_name, $prefix);
      $updates = array_merge($updates, $compare_results['updates']);
      $news = array_merge($news, $compare_results['new']);
      $skipped = $skipped + $compare_results['skipped'];
      $skip = $compare_results['skip'];

      var_dump([
        'news' => count($news),
        'updates' => count($updates),
        'skip' => count($skip),
      ]);
    }
  }


  $result_count = count($updates) + count($news) + $skipped;
  array_unique(array_column($updates, 'id'));
  $ids = [];
  foreach ($updates as $key => $update) {
    if (!in_array($update['id'], $ids)) {
      $ids[] = $update['id'];
    } else {
      unset($updates[$key]);
    }
  }

  unset($compare_results);

  $modelname = 'App\\' . $database;
  $base_obj = new $modelname();

  $to_log['updated'] = count($updates);
  $to_log['new'] = count($news);

  $to_file = $news;
  usort($to_file, function ($a, $b) {
    return $a['volume'] < $b['volume'];
  });
  $str = '';
  foreach ($to_file as $fields) {
    $str .= join(',', $fields) . "\n";
  }
  unset($to_file);
  $str = mb_convert_encoding(trim($str),'UTF-16LE','UTF-8');

  file_put_contents($filename, $str);

  $news_chunked = array_chunk($news, 1000);
  unset($news);
  foreach ($news_chunked as $key => $new) {
    $base_obj->insert($new);
    unset($new);
  }
  $ids_for_delete = array_column($updates, 'id');
  $count_for_del = $base_obj->whereIn('id', $ids_for_delete)->delete();
  $updates_chunked = array_chunk($updates, 1000);
  unset($updates);
  foreach ($updates_chunked as $key => $update) {
    $base_obj->insert($update);
    unset($update);
  }
}
