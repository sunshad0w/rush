<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
set_time_limit(0);
require "../vendor/autoload.php";

use App\Negative;
use Hhxsv5\PhpMultiCurl\Curl;
use Hhxsv5\PhpMultiCurl\MultiCurl;
use Illuminate\Database\Capsule\Manager as Capsule;

//$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
//socket_bind($socket, '127.0.0.1', 99999);
$sock = socket_create_listen(9999);

$capsule = new Capsule;
$capsule->addConnection([
  "driver" => "mysql",
  "host" =>"158.85.72.153",
  "database" => "semrush",
  "username" => "semrush",
  "password" => "semRush_2018_",
  'charset'   => 'utf8',
  'collation' => 'utf8_unicode_ci',
]);
//Make this Capsule instance available globally.
$capsule->setAsGlobal();

// Setup the Eloquent ORM.
$capsule->bootEloquent();
///////////////////////////////////

$models = [
  'Download',
'Email',
'Form',
'Map',
'Manual',
'Negative',
'Pdf',
'Recipe',
];

//negatives = array_map('mb_strtolower', array_map('trim', Negative::all()->pluck('keyword')->toArray()));
$negatives = array_map('mb_strtolower', array_map('trim', Negative::whereIn('id',[183930,184136])->pluck('keyword')->toArray()));

$chunk_size = 250000;
foreach ($models as $model) {
  $model_name = 'App\\'.$model;
  $base_obj = new $model_name();
  $chunk_count = ceil($base_obj::count() / $chunk_size);
  $start_id = 0;
  $to_del = [];
  for ($i = 1; $i <= $chunk_count; $i++) {
    echo "chunk $i / $chunk_count\n";
    $data = $base_obj::where('id', '>', $start_id)->limit($chunk_size)->get()->toArray();
    $start_id = last($data)['id'];
    $s = count($negatives);
    foreach ($negatives as $key => $negative) {
      echo "negative $key / $s\n";
      foreach ($data as $row) {
        if (preg_match('#(^| )'.preg_quote($negative).'( |$)#i', mb_strtolower(trim($row['keyword'])))){
          $to_del[$row['id']] = ['db_k'=>$row['keyword'], 'neg_k'=>$negative];
        }
      }
    }
  }
  echo "writen $model\n";
  file_put_contents(__DIR__ . '/1234/' . $model . '_dels.php', '<?php return ' . var_export($to_del, true) . ';');
}
