<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
set_time_limit(0);
require "../vendor/autoload.php";

use App\Negative;
use Hhxsv5\PhpMultiCurl\Curl;
use Hhxsv5\PhpMultiCurl\MultiCurl;
use Illuminate\Database\Capsule\Manager as Capsule;

//$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
//socket_bind($socket, '127.0.0.1', 99999);
$sock = socket_create_listen(9999);

$capsule = new Capsule;
$capsule->addConnection([
  "driver" => "mysql",
  "host" =>"158.85.72.153",
  "database" => "semrush",
  "username" => "semrush",
  "password" => "semRush_2018_",
  'charset'   => 'utf8',
  'collation' => 'utf8_unicode_ci',
]);
//Make this Capsule instance available globally.
$capsule->setAsGlobal();

// Setup the Eloquent ORM.
$capsule->bootEloquent();

$request_body = file_get_contents('php://input');
$data = json_decode($request_body, true);

$api_key = '0ad9941199354eb97cf5dc7ce758bc5f';
if($data['country'] !== ''){
  $country_code = strtolower($data['country']['code']);
  $country_name = strtolower($data['country']['name']);
}

$domain = $data['domain'];
$database = $data['database'];
$nqMin = $data['NqMin'];
$nqMax = $data['NqMax'];
$wordMax = $data['wordMax'];
$charMax = $data['charMax'];

$maxKeywords = $data['maxKeywords'];
$offset = $data['offset'];
$urls = [];

$url= 'https://api.semrush.com/?type=domain_organic&key='.$api_key;

$url .= '&domain='.$domain;

$url .= '&export_columns=Ph,Nq';
$url .= '&display_sort=tr_desc';
if (isset($country_code)) $url = $url.'&database='.$country_code;
$display_filter = '+|Nq|Gt|'.($nqMin-1);


if( $nqMax !== '' && $nqMax !== null) $display_filter .= '|+|Nq|Lt|'.($nqMax+1);
$display_filter = urlencode($display_filter);
$url .= '&display_filter=' . $display_filter;


$start_pos = 0;
if( $offset !== '' && $offset !== null) {
  $start_pos = $offset;
  //$url .= '&display_offset='.$offset;
} else {
  $start_pos = 0;
  //$offset = 0;
}

$end_pos = 0;
if( $maxKeywords !== '' && $maxKeywords !== null) {
  $end_pos = $maxKeywords+$offset;
  //$url .= '&display_limit='.($maxKeywords+$offset);
} else {
  $end_pos = 10+$offset;
  //$url .= '&display_limit='.(10+$offset);
}

$max_per_request = 50000;

for ($i = 0; $i < ceil(($end_pos - $start_pos) / $max_per_request); $i++){
  $urls[] = $url.'&display_offset='.($offset+$i*$max_per_request).'&display_limit='.(($offset+($i+1)*$max_per_request)>$end_pos?$end_pos:($offset+($i+1)*$max_per_request));
}


$options = [//The custom options of cURL
  CURLOPT_TIMEOUT        => 0,
  CURLOPT_CONNECTTIMEOUT => 0,
];


$curls = [];
foreach ($urls as $url) {
  $curl = new Curl(null, $options);
  $curl->makeGet($url);
  $curls[] = $curl;
}

$mc = new MultiCurl();
$mc->addCurls($curls);
$allSuccess = $mc->exec();

$native_content = '';
$content = '';
$errors = '';

if ($allSuccess) {
  //All success
  foreach ($curls as $key => $curl) {
    $body = $curl->getResponse()->getBody();
    if ($key > 0){
      if (!preg_match('/ERROR.*/',$content)){
        $content .=  "\n".preg_replace('/^.+\n/', '', $body);
      }
      $native_content .= "\n" . preg_replace('/^.+\n/', '', $body);
    } else {
      if (!preg_match('/ERROR.*/',$content)){
        $content .= $body;
      }
      $native_content .=  $body;
    }
  }
} else {
  //Some curls failed
  foreach ($curls as $curl) {
    var_dump($curl->getResponse()->getError());
  }
}
$error_filename ='/csvs/errors/'.$database.'_'.$domain.'('.date('d_m_Y H_i_s') . ').txt';
if(strlen($errors)) file_put_contents( dirname(__DIR__).$error_filename, $errors);

file_put_contents( dirname(__DIR__).'/csvs/requests/'.$database.'_'.$domain.'('.date('d_m_Y H_i_s') . ').txt', join("\n", $urls));

$modelname = 'App\\'.$database;
$base_obj = new $modelname();

//$content = file_get_contents($url);
if (preg_match('/ERROR.*/',$native_content) && !preg_match('/NOTHING FOUND/',$native_content)){
  echo json_encode(['error'=>$native_content]);
  exit();
}

$only_part = false;
if (preg_match('/ERROR 50/',$native_content)){
  $only_part = true;
}

$native_filename ='/csvs/native_data/'.$database.'_'.$domain.'('.date('d_m_Y H_i_s') . ').txt';
file_put_contents( dirname(__DIR__).$native_filename, $native_content);
unset($native_content);

$result = explode("\n",$content);
$result = array_unique($result);
unset($content);
$result = array_map('trim', $result);

$filename = '/csvs/'.$database.'_'.$domain.'('.date('d_m_Y H_i_s') . ').csv';

if (count($result)>1){
  $negatives = array_map('sha1',array_map('trim',Negative::all()->pluck('keyword')->toArray()));
}

$exsisting = $base_obj::where('country','=',$country_name)->get();
$ex = [];
foreach ($exsisting as $item) {
  $ex[sha1(trim($item->keyword))] = [
    'id'=>$item->id,
    'volume'=>$item->volume,
    'created_at'=>$item->created_at,
    'keyword'=>$item->keyword,
    'country'=>$item->country
  ];
}
unset($exsisting);

$to_log = [];
$to_file = [];
$news = [];
$updates = [];
$result_count = count($result)-1;
//$result_chanked = array_chunk($result, 1000);

foreach ($result as $key => $row) {
  if ($key == 0) continue;
  $rush_data = array_map('trim',explode(';',$row));
  if (count($rush_data) !== 2) continue;
  $keyword = $rush_data[0];
  $hash = sha1($keyword);
  $volume = $rush_data[1];
  if (in_array($hash, $negatives)) continue;
  $key_info = [
    'keyword' => $keyword,
    'country'=> $country_name,
    'volume'=> $volume
  ];
  if (isset($ex[$hash])){
    if($ex[$hash]['volume']!=$volume){
      $updates[] = [
        'id'=>$ex[$hash]['id'],
        'volume'=>$volume,
        'created_at'=>$ex[$hash]['created_at'],
        'keyword'=>$ex[$hash]['keyword'],
        'country'=>$ex[$hash]['country']
      ];
    }
  } else {
    $news[] = $key_info;
  }
  }

$count_updates = count($updates);
$to_log['updated'] = $count_updates;
$count_news = count($news);
$to_log['new'] = $count_news;
$to_file = $news;
$news_chunked = array_chunk($news, 1000);
unset($news);
foreach ($news_chunked as $key => $new){
  $base_obj->insert($new);
  unset($new);
}

$ids_for_delete = array_column($updates, 'id');
$count_for_del = $base_obj->whereIn('id',$ids_for_delete)->delete();
$updates_chunked = array_chunk($updates, 1000);
unset($updates);
foreach ($updates_chunked as $key => $update){
  $base_obj->insert($update);
  unset($update);
}

usort($to_file, function ($a, $b){return $a['volume']<$b['volume'];});
$str = '';
foreach ($to_file as $fields) {
  $str .= join(',', $fields) . "\n";
}
$str = trim($str);
file_put_contents(dirname(__DIR__).$filename, $str);

if (filesize(dirname(__DIR__).$filename)/1024/1024 > 5){
  $command = "zip -j \"".dirname(__DIR__).$filename.".zip\" \"" . dirname(__DIR__) . $filename."\"";
  $stdout = exec($command);
  $mail_content = file_get_contents(dirname(__DIR__) . $filename . ".zip");
} else {
  $mail_content = $str;
}
$emailAddress = 'avigoldfinger@gmail.com';
$emailAddress = 'sunshad0w.0leg@gmail.com';
$separator = md5(time());
$eol       = "\r\n";

$message = "--" . $separator . $eol;
$message .= "Content-Type: text/html; charset=\"iso-8859-1\"" . $eol;
$message .= "Content-Transfer-Encoding: 8bit" . $eol;

$message .= $database.'_'.$domain.'('.date('d_m_Y H_i_s').') data'. $eol;
$message .= "STATUS\nRequested results: ".$maxKeywords."\nReceived results: ".$result_count. $eol;
if (strlen($mail_content)>0){
  $mail_content = chunk_split(base64_encode($mail_content));
  $message .= "--" . $separator . $eol;
  $message .= "Content-Type: application/octet-stream; name=\"".$filename."\"" . $eol;
  $message .= "Content-Transfer-Encoding: base64" . $eol;
  $message .= "Content-Disposition: attachment" . $eol;
  $message .= $mail_content . $eol;
  $message .= "--" . $separator . "--";
} else {
  $message .= 'no new record'. $eol;
}

$headers = 'From: info@bngguitars.com' . $eol .
  //'Reply-To: info@bngguitars.com' . "\r\n" .
  'X-Mailer: PHP/' . phpversion();
$headers .= "MIME-Version: 1.0" . $eol;
$headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol;
$mail = mail($emailAddress, 'semrush '.$database.'_'.$domain.'('.date('d_m_Y H_i_s').')', $message, $headers);


//fclose($fp);

echo json_encode(array_merge($to_log, [
  'filename'=>$filename,
  'native_data'=> $native_filename,
  //'string'=>trim($str),
  'url'=>$url,
  'total_count' => count($result) - 1,
]));
//socket_close($sock);
