<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
set_time_limit(0);
require "../vendor/autoload.php";

use App\Negative;
use Hhxsv5\PhpMultiCurl\Curl;
use Hhxsv5\PhpMultiCurl\MultiCurl;
use Illuminate\Database\Capsule\Manager as Capsule;

//$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
//socket_bind($socket, '127.0.0.1', 99999);
$sock = socket_create_listen(9999);

$capsule = new Capsule;
$capsule->addConnection([
  "driver" => "mysql",
  "host" =>"158.85.72.153",
  "database" => "semrush",
  "username" => "semrush",
  "password" => "semRush_2018_",
  'charset'   => 'utf8',
  'collation' => 'utf8_unicode_ci',
]);
//Make this Capsule instance available globally.
$capsule->setAsGlobal();

// Setup the Eloquent ORM.
$capsule->bootEloquent();
///////////////////////////////////

$list = array_diff(scandir(__DIR__ . '/123'), ['.','..']);
foreach ($list as $item) {
  $database = explode('_', $item)[0];
  $model_name = 'App\\'.$database;
  $base_obj = new $model_name();

  $arr = include(__DIR__ . '/123/' . $item);
  $ids_for_delete = array_keys($arr);

  //var_dump($database, count($ids_for_delete));
  //exit();
  $dels = array_chunk($ids_for_delete, 10000);
  foreach ($dels as $del) {
    $count_for_del = $base_obj->whereIn('id',$del)->delete();
    var_dump('in ' . $database . ' deleted: ' . $count_for_del);
  }

}
//var_dump($list);





exit();
$database = 'Download1';
$domain = 'softonic.com';
$country_name = 'United States';
///////////////////////////////////

$native_filename = '/csvs/native_data/' . $database . '_' . $domain . '(' . date('d_m_Y H_i_s') . ').txt';
$error_filename = '/csvs/errors/' . $database . '_' . $domain . '(' . date('d_m_Y H_i_s') . ').txt';
$filename = '/csvs/' . $database . '_' . $domain . '(' . date('d_m_Y H_i_s') . ').csv';

$to_log = [];
$to_file = [];

$prefix = $database.'_'.ceil(time()/2720)*2720;

function compareWithTable($rush_data, $negatives, $database, $country_name, $prefix){
  $chunk_size = 250000;
  $modelname = 'App\\'.$database;
  $base_obj = new $modelname();

  $skip_count = 0;

  $tmp = [];
  foreach ($rush_data as $key => $rush_datum) {
    if($key == 0) {
      $skip_count++;
      continue;
    };
    $rush_row_data = array_map('trim',explode(';',$rush_datum));
    if (count($rush_row_data) !== 2) {
      $skip_count++;
      continue;
    };
    $tmp[] = ['keyword'=>$rush_row_data[0],'country'=>$country_name,'volume'=>$rush_row_data[1]];
  }
  $rush_data = $tmp;
  unset($tmp);

  $updates = [];

  $chunk_count = ceil($base_obj::where('country','=',$country_name)->count()/$chunk_size);
  $start_id = 0;
  for( $i = 1; $i<= $chunk_count; $i++ ){
    $cache_file_name = __DIR__ . '/tmp/' . $prefix . '_' . $i;
    if(file_exists($cache_file_name)){
      $data = include($cache_file_name);
    } else {
      $data = $base_obj::where('country','=',$country_name)->where('id','>',$start_id)->limit($chunk_size)->get()->toArray();
      file_put_contents($cache_file_name,"<?php return ".var_export($data, true).";");
    }
    $start_id = last($data)['id'];
    $ex = [];
    foreach ((object)$data as $item) {
      $ex[sha1(trim($item['keyword']))] = [
        'id'=>$item['id'],
        'volume'=>$item['volume'],
        'created_at'=>$item['created_at'],
        'keyword'=>$item['keyword'],
        'country'=>$item['country']
      ];
    }
    unset($data);

    $news = [];

    foreach ($rush_data as $key => $rush_row_data) {
      $hash = sha1($rush_row_data['keyword']);
      if (in_array($hash, $negatives)) {
          var_dump($hash, $rush_row_data['keyword'],"====");
        $skip_count++;
        continue;
      };
      if (isset($ex[$hash])){
        if($ex[$hash]['volume']!=$rush_row_data['volume']){
          $updates[] = [
            'id'=>$ex[$hash]['id'],
            'volume'=>$rush_row_data['volume'],
            'created_at'=>$ex[$hash]['created_at'],
            'keyword'=>$ex[$hash]['keyword'],
            'country'=>$ex[$hash]['country']
          ];
        } else {
          $skip_count++;
          continue;
        }
      } else {
        $news[] = $rush_row_data;
      }
    }
    $rush_data = $news;

  }

  for( $i = 1; $i<= $chunk_count; $i++ ){
    $cache_file_name = __DIR__ . '/tmp/' . $prefix . '_' . $i;
    if(file_exists($cache_file_name)){
      //unlink($cache_file_name);
    }
  }

  return ['new'=>$rush_data, 'updates'=>$updates, 'skipped'=>$skip_count];
}


$body = file_get_contents(__DIR__.'/ntmp.txt');
$result = array_unique(array_map('trim', explode("\n",$body)));
$negatives = array_map('strtolower',array_map('trim',Negative::all()->pluck('keyword')->toArray()));


$compare_results = compareWithTable($result, $negatives, $database, $country_name, $prefix);
var_dump('compared');
$result_count = count($compare_results['updates']) + count($compare_results['new']) + $compare_results['skipped'];
var_dump(count($compare_results['updates']), count($compare_results['new']), $compare_results['skipped']);

$model_name = 'App\\'.$database;
$base_obj = new $model_name();

$news_chunked = array_chunk($compare_results['new'], 1000);
foreach ($news_chunked as $key => $new){
  var_dump('insert '.($key+1));
  //$base_obj->insert($new);
  unset($new);
}

$ids_for_delete = array_column($compare_results['updates'], 'id');
//$count_for_del = $base_obj->whereIn('id',$ids_for_delete)->delete();
var_dump('deleted ' . $count_for_del);
file_put_contents('updates', var_export($compare_results['updates'], true));

$updates_chunked = array_chunk($compare_results['updates'], 1000);
unset($updates);
foreach ($updates_chunked as $key => $update){
  var_dump('update - insert '.($key+1));
  //$base_obj->insert($update);
  unset($update);
}


$to_file = $compare_results['new'];
usort($to_file, function ($a, $b){return $a['volume']<$b['volume'];});
$str = '';
foreach ($to_file as $fields) {
  $str .= join(',', $fields) . "\n";
}
$str = trim($str);
file_put_contents(dirname(__DIR__).$filename, $str);

if (filesize(dirname(__DIR__).$filename)/1024/1024 > 5){
  $command = "zip -j \"".dirname(__DIR__).$filename.".zip\" \"" . dirname(__DIR__) . $filename."\"";
  $stdout = exec($command);
  $mail_content = file_get_contents(dirname(__DIR__) . $filename . ".zip");
} else {
  $mail_content = $str;
}
