<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
set_time_limit(0);
require "../vendor/autoload.php";

use App\Negative;
use Hhxsv5\PhpMultiCurl\Curl;
use Hhxsv5\PhpMultiCurl\MultiCurl;
use Illuminate\Database\Capsule\Manager as Capsule;

//$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
//socket_bind($socket, '127.0.0.1', 99999);
$sock = socket_create_listen(9999);

$capsule = new Capsule;
$capsule->addConnection([
    "driver" => "mysql",
    "host" => "158.85.72.153",
    "database" => "semrush",
    "username" => "semrush",
    "password" => "semRush_2018_",
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
]);
//Make this Capsule instance available globally.
$capsule->setAsGlobal();

// Setup the Eloquent ORM.
$capsule->bootEloquent();

$request_body = file_get_contents('php://input');
$data = json_decode($request_body, true);

$database = 'Download';
$country_name = 'United States';
$domain = 'us';
$to_log = [];
$to_file = [];

$prefix = $database . '_' . (round(time() / 60) * 60);


function compareWithTable($rush_data, $negatives, $database, $country_name, $prefix)
{
  $skips = [];
    $chunk_size = 250000;
    $modelname = 'App\\' . $database;
    $base_obj = new $modelname();

    $skip_count = 0;

    $tmp = [];
    foreach ($rush_data as $key => $rush_datum) {
        if ($key == 0) {
            $skip_count++;
            continue;
        };
        $rush_row_data = array_map('trim', explode(';', $rush_datum));
        if (count($rush_row_data) !== 2) {
            //var_dump('columns skip',$rush_row_data);
            //$skips[] = ['reason'=>'column count','data'=>$rush_row_data];
            $skip_count++;
            continue;
        };
        $tmp[] = ['keyword' => $rush_row_data[0], 'country' => $country_name, 'volume' => $rush_row_data[1]];
    }
    $rush_data = $tmp;
    unset($tmp);

    $updates = [];

    $chunk_count = ceil($base_obj::where('country', '=', $country_name)->count() / $chunk_size);
    $start_id = 0;
    for ($i = 1; $i <= $chunk_count; $i++) {
        $cache_file_name = __DIR__ . '/tmp/' . $prefix . '_' . $i;
        if (file_exists($cache_file_name)) {
            $data = include($cache_file_name);
        } else {
            $data = $base_obj::where('country', '=', $country_name)->where('id', '>', $start_id)->limit($chunk_size)->get()->toArray();
            file_put_contents($cache_file_name, "<?php return " . var_export($data, true) . ";");
        }
        $start_id = last($data)['id'];
        $ex = [];
        foreach ((object)$data as $item) {
            $ex[mb_strtolower(trim($item['keyword']))] = [
                'id' => $item['id'],
                'volume' => $item['volume'],
                'created_at' => $item['created_at'],
                'keyword' => $item['keyword'],
                'country' => $item['country']
            ];
        }
        unset($data);

        $news = [];

        foreach ($rush_data as $key => $rush_row_data) {
            $hash = mb_strtolower($rush_row_data['keyword']);
            $skip = false;
            $negatived = '';
            foreach ($negatives as $negative) {
                  if (!$skip && preg_match('#(^|\n|\r|\s)' . $negative . '(\:|\?|\;|\.|\s|\,|\n|\r)#i', $hash)) {
                  $negatived = $negative;
                    $skip = true;
                }
            }
            if ($skip) {
                $skip_count++;
                if (!isset($skips[$negatived])) {
                  $skips[$negatived] = 1;
                } else {
                  $skips[$negatived]++;
                }
                $skips[] = ['reason'=>'negative skip','keyword'=>$rush_row_data['keyword'], 'negative'=>$negatived];

                continue;
            };
            if (isset($ex[$hash])) {
                if ($ex[$hash]['volume'] != $rush_row_data['volume']) {
                    $updates[] = [
                        'id' => $ex[$hash]['id'],
                        'volume' => $rush_row_data['volume'],
                        'created_at' => $ex[$hash]['created_at'],
                        'keyword' => $ex[$hash]['keyword'],
                        'country' => $ex[$hash]['country']
                    ];
                } else {
                  //$skips[] = ['reason'=>'volume skip','data'=>$rush_row_data];
                  //var_dump('volume skip',$rush_row_data);
                    $skip_count++;
                    continue;
                }
            } else {
                $news[] = $rush_row_data;
            }
        }
        $rush_data = $news;
    }

    for ($i = 1; $i <= $chunk_count; $i++) {
        $cache_file_name = __DIR__ . '/tmp/' . $prefix . '_' . $i;
        if (file_exists($cache_file_name)) {
            unlink($cache_file_name);
        }
    }

    return ['new' => $rush_data, 'updates' => $updates, 'skipped' => $skip_count, 'skip'=>$skips];
}

//$native_filename_relative = '/csvs/native_data/' . $database . '_' . $domain . '(' . date('d_m_Y H_i_s') . ').txt';
//$native_filename = dirname(__DIR__) . $native_filename_relative;
//$error_filename = dirname(__DIR__) . '/csvs/errors/' . $database . '_' . $domain . '(' . date('d_m_Y H_i_s') . ').txt';
$filename_relative = '/csvs/' . $database . '_' . $domain . '(' . date('d_m_Y H_i_s') . ').csv';
$filename = dirname(__DIR__) . $filename_relative;

$updates = [];
$news = [];
$skipped = 0;

$only_part = '';
if (true) {
  $body = trim(file_get_contents('Download_download(29_03_2019 07_43_37).txt'));
  if (preg_match('/ERROR.*/', $body)) {
    preg_match('/ERROR (\d+)/', $body, $match);
    if (count($match) > 1) $only_part = $match[1];
    var_dump('error');
    //file_put_contents($error_filename, $body . "\n", FILE_APPEND);
  } else if (preg_match('/\<\!DOCTYPE html\>/', $body)) {
    preg_match('#\<title\>(.*)<\/title\>#', $body, $match);
    if (count($match)>1) $only_part = $match[1];
    //file_put_contents($error_filename, $body . "\n", FILE_APPEND);
    var_dump('error');
  } else {
    //file_put_contents($native_filename, $body . "\n", FILE_APPEND);

    $result = array_unique(array_map('mb_strtolower', array_map('trim', explode("\n", $body))));
    unset($body);

    if (count($result) > 1) {
      $negatives = array_map('mb_strtolower', array_map('trim', Negative::all()->pluck('keyword')->toArray()));

      $compare_results = compareWithTable($result, $negatives, $database, $country_name, $prefix);
      $updates = array_merge($updates, $compare_results['updates']);
      $news = array_merge($news, $compare_results['new']);
      $skipped = $skipped + $compare_results['skipped'];
      $skip = $compare_results['skip'];
    }
  }

    $result_count = count($updates) + count($news) + $skipped;
    array_unique(array_column($updates, 'id'));
    $ids = [];
    foreach ($updates as $key => $update) {
        if (!in_array($update['id'], $ids)) {
            $ids[] = $update['id'];
        } else {
            unset($updates[$key]);
        }
    }

    unset($compare_results);

    $modelname = 'App\\' . $database;
    $base_obj = new $modelname();

    $to_log['updated'] = count($updates);
    $to_log['new'] = count($news);

    $to_file = $news;
    usort($to_file, function ($a, $b) {
        return $a['volume'] < $b['volume'];
    });
    $str = '';
    foreach ($to_file as $fields) {
        $str .= join(',', $fields) . "\n";
    }
    unset($to_file);
    $str = mb_convert_encoding(trim($str),'UTF-16LE','UTF-8');

    file_put_contents($filename, $str);

    $news_chunked = array_chunk($news, 1000);
    unset($news);
    foreach ($news_chunked as $key => $new) {
        $base_obj->insert($new);
        unset($new);
    }
    $ids_for_delete = array_column($updates, 'id');
    $count_for_del = $base_obj->whereIn('id', $ids_for_delete)->delete();
    $updates_chunked = array_chunk($updates, 1000);
    unset($updates);
    foreach ($updates_chunked as $key => $update) {
        $base_obj->insert($update);
        unset($update);
    }

  exit('done');

    if (filesize($filename) / 1024 / 1024 > 5) {
        $command = "zip -j \"" . $filename . ".zip\" \"" . $filename . "\"";
        $stdout = exec($command);
        $mail_content = file_get_contents($filename . ".zip");
    } else {
        $mail_content = $str;
    }
    $emailAddress = 'avigoldfinger@gmail.com';
    $emailAddress = 'sunshad0w.0leg@gmail.com';
    $separator = md5(time());
    $eol = "\r\n";

    $message = "--" . $separator . $eol;
    $message .= "Content-Type: text/html; charset=\"iso-8859-1\"" . $eol;
    $message .= "Content-Transfer-Encoding: 8bit" . $eol;

    $message .= $database . '_' . $domain . '(' . date('d_m_Y H_i_s') . ') data<br>' . $eol;
    $message .= "STATUS\nRequested results: " . $maxKeywords . "\n<br>Received results: " . $result_count .'<br>'. $eol;
    if ($only_part !== ''){
      $message .= "noticed error: $only_part<br>". $eol;
    }

    if (strlen($mail_content) > 0) {
        $mail_content = chunk_split(base64_encode($mail_content));
        $message .= "--" . $separator . $eol;
        $message .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"" . $eol;
        $message .= "Content-Transfer-Encoding: base64" . $eol;
        $message .= "Content-Disposition: attachment" . $eol;
        $message .= $mail_content . $eol;
        $message .= "--" . $separator . "--";
    } else {
        $message .= 'no new record' . $eol;
    }

    $headers = 'From: info@bngguitars.com' . $eol .
        //'Reply-To: info@bngguitars.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
    $headers .= "MIME-Version: 1.0" . $eol;
    $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol;
    $mail = mail($emailAddress, 'semr1ush ' . $database . '_' . $domain . '(' . date('d_m_Y H_i_s') . ')', $message, $headers);


//fclose($fp);

    echo json_encode(array_merge($to_log, [
        'filename' => $filename_relative,
        'native_data' => $native_filename_relative,
        //'string'=>trim($str),
        //'urls' => $urls,
        'total_count' => $result_count,
    ]));


} else {
    //Some curls failed
    foreach ($curls as $curl) {
        file_put_contents($error_filename, var_export($curl->getResponse()->getError(), true), FILE_APPEND);
        //var_dump($curl->getResponse()->getError());
    }
}



