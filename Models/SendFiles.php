<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class SendFiles extends Eloquent
{
  protected $fillable = ['keyword','country','volume'];
  protected $table = 'sendfiles';
}
