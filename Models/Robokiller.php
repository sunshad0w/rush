<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Robokiller extends Eloquent
{
  protected $fillable = ['keyword','country','volume'];
  protected $table = 'robokillers';
}
