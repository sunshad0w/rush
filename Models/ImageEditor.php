<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ImageEditor extends Eloquent
{
  protected $fillable = ['keyword','country','volume'];
  protected $table = 'image_editor';
}
