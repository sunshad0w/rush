<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Speedtest extends Eloquent
{
  protected $fillable = ['keyword','country','volume'];
  protected $table = 'speedtest';
}
