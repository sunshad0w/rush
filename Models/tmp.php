<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class tmp extends Eloquent
{
  protected $fillable = ['keyword','country','volume'];
  protected $table = 'tmp';
}
