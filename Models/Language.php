<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Language extends Eloquent
{
  protected $fillable = ['keyword','country','volume'];
  protected $table = 'language';
}
