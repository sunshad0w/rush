<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Download1 extends Eloquent
{
  protected $fillable = ['keyword','country','volume'];
  protected $table = 'downloads1';
}
