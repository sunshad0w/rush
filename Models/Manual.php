<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Manual extends Eloquent
{
  protected $fillable = ['keyword','country','volume'];
  protected $table = 'manuals';

}
