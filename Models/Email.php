<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Email extends Eloquent
{
  protected $fillable = ['keyword','country','volume'];
  protected $table = 'email';

}
