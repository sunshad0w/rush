<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Recipe extends Eloquent
{
  protected $fillable = ['keyword','country','volume'];
  protected $table = 'recipes';
}
