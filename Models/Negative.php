<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Negative extends Eloquent
{
  protected $fillable = ['keyword'];
  protected $table = 'negatives';
}
