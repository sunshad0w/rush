<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Download extends Eloquent
{
  protected $fillable = ['keyword','country','volume'];
  protected $table = 'downloads';
}
