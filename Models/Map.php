<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Map extends Eloquent
{
  protected $fillable = ['keyword','country','volume'];
  protected $table = 'maps';

}
