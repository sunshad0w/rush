import {Component, OnInit} from '@angular/core';
import {rushData} from "../rushData";
import {logData} from "../logData";
import {HttpService} from "../http.service";
import {LogService} from "../log.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'rush-form',
  templateUrl: './rush-form.component.html',
  styleUrls: ['./rush-form.component.scss'],
})
export class RushFormComponent implements OnInit {
  requestForm: FormGroup;
  submitted = false;
  rush: rushData=new rushData();
  countries = require('../countries.json');

  constructor(private httpService: HttpService, private formBuilder: FormBuilder, private logService: LogService) {
  }

  ngOnInit() {
    this.requestForm = this.formBuilder.group({
      keywordMode: [false],
      subdomainMode: [false],
      table: ['Download', Validators.required],
      country: ['',[Validators.required]],
      domain: ['', [Validators.required, Validators.minLength(6)]],
      keyword: [''],
      NqMin: ['1', [Validators.required, Validators.min(1)]],
      offset: ['0', [Validators.required, Validators.min(0)]],
      NqMax: [''],
      constants: [false],
      maxKeywords: ['20', [Validators.required, Validators.min(1), Validators.max(5000000)]],
      wordMax: ['', Validators.min(1)],
      charMax: ['', Validators.min(1)]
    });

    this.keywordMode.valueChanges.subscribe(checked => {
      if(checked){
        this.domain.setValidators(null);
        this.keyword.setValidators([Validators.required, Validators.minLength(2)]);
      } else {
        this.domain.setValidators([Validators.required, Validators.minLength(6)]);
        this.keyword.setValidators(null);
      }
      this.domain.updateValueAndValidity();
      this.keyword.updateValueAndValidity();
    });

  }

  get keywordMode() {
    return this.requestForm.get('keywordMode') as FormControl;
  }

  get domain() {
    return this.requestForm.get('domain') as FormControl;
  }

  get keyword() {
    return this.requestForm.get('keyword') as FormControl;
  }

  get f() { return this.requestForm.controls; }


  onSubmit(){
    let data = this.f;
    let fields = Array.from(document.querySelectorAll('form>ng-select'));
    fields = fields.concat(Array.from(document.querySelectorAll('form input')));

    for (let prop in fields){
      if (typeof fields[prop] === 'object'){
        fields[prop].classList.add("ng-dirty")
      }
    }

    this.submitted = true;
    if (this.requestForm.invalid) {
      console.log('not valid form');
      return;
    }

    for (let item in data){
      this.rush[item] = data[item].value;
    }

    this.httpService.post(this.rush).subscribe(result => {
      this.logService.logProvider.next(new logData(result));
    });

  }
}
