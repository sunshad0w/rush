export class logData{
  filename: string;
  native_data: string;
  error: string;
  total_count: number;
  new: any;
  updated: any;

  constructor(obj?: any) {
    Object.assign(this, obj);
  }
}
