import { Component, OnInit } from '@angular/core';
import {logData} from "../logData";
import {LogService} from "../log.service";

@Component({
  selector: 'app-logger',
  templateUrl: './logger.component.html',
  styleUrls: ['./logger.component.scss']
})
export class LoggerComponent implements OnInit {
  logData : logData;

  constructor(private logService : LogService) { }

  ngOnInit() {
    this.logService.logProvider.subscribe((logData)=>{
      this.logData = logData;
    })
  }

}
