export class rushData{
  keywordMode: boolean;
  subdomainMode: boolean;
  constants: boolean;
  country: any;
  table: string;
  keyword: string;
  domain: string;
  NqMin: number;
  NqMax: number;
  maxKeywords: number;
  offset: number;
  wordMax: any;
  charMax: any;
}
