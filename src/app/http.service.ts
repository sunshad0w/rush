import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {rushData} from "./rushData";

@Injectable()
export class HttpService{

  constructor(private http: HttpClient){ }

  post(rush: rushData, url = '/api/'){
    return this.http.post(url, rush);
  }

  get(url){
    return this.http.get(url);
  }
}
