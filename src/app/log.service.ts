import {Injectable} from '@angular/core';
import {logData} from "./logData";
import {Subject} from "rxjs/internal/Subject";

@Injectable()
export class LogService{
  logProvider = new Subject<logData>();
}
