import { Component } from '@angular/core';
import {HttpService} from "../http.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  //title = 'rush-collector';
  show_log=false;
  status = '';

  constructor(private httpService: HttpService) {
    setInterval(()=>{
      this.httpService.get('/api/ping.php').subscribe(result => {
        let data : any;
        data = result;
        if (!data.socket) {
          this.status = 'no active process';
        } else {
          this.status = '..processing';
        }
      });
    }, 1000);
  }

  toggle_log = () => {
    this.show_log = !this.show_log;
  }
}
