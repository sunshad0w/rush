import { BrowserModule } from '@angular/platform-browser';
import {ReactiveFormsModule} from "@angular/forms";
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import { UiSwitchModule } from 'ngx-ui-switch';
import { NgSelectModule} from '@ng-select/ng-select';

//import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app-root/app.component';
import { RushFormComponent } from './rush-form/rush-form.component';
import { LoggerComponent } from './logger/logger.component';
import {HttpService} from "./http.service";
import {LogService} from "./log.service";

@NgModule({
  declarations: [
    AppComponent,
    RushFormComponent,
    LoggerComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
//    AppRoutingModule,
    NgSelectModule,
    UiSwitchModule.forRoot({
      size: 'medium',
      color: 'rgb(0, 189, 99)',
      switchColor: '#60a4ff',
      defaultBgColor: '#fff',
      defaultBoColor : '#cfc',
      checkedLabel: 'constants on',
      uncheckedLabel: 'constants off'
    })
  ],
  providers: [HttpService, LogService],
  bootstrap: [AppComponent]
})
export class AppModule { }
